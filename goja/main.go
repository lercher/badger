package main

import (
	"io/ioutil"
	"log"

	"github.com/dop251/goja"
)

const (
	tstgz        = `https://github.com/microsoft/TypeScript/releases/download/v3.7.2/typescript-3.7.2.tgz`
	typescriptjs = `package/lib/typescriptServices.js`
)

func main() {
	log.SetFlags(log.Ltime | log.Lshortfile)
	log.Println("creating new goja engine")
	js := goja.New()

	log.Println("loading typescript")
	ts, err := ioutil.ReadFile(`typescriptServices372.js`)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("executing ts")
	_, err = js.RunScript("typescript", string(ts))
	if err != nil {
		log.Fatalln(err)
	}
	tsver, err := js.RunScript("", "ts.version;")
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Typescript Version", tsver.Export().(string))
	log.Println()

	log.Println("transpile function")
	// https://www.typescriptlang.org/docs/handbook/compiler-options.html at '--lib' for ts.ScriptTarget.ES5,
	// goja supports ES5.1 so we choose ES5 as target
	// export function transpile(input: string, compilerOptions?: CompilerOptions, fileName?: string, diagnostics?: Diagnostic[], moduleName?: string): string;
	_, err = js.RunScript("transpileFunc", `
	function transpile(filename, src) { 
		return ts.transpile(
			src, 
			{ 
				target: ts.ScriptTarget.ES5, 
				inlineSourceMap: false 
			}, 
			filename
		)
	};`)
	if err != nil {
		log.Fatalln(err)
	}

	var transpile func(string, string) string
	js.ExportTo(js.Get("transpile"), &transpile)
	
	log.Println("transpiling script")
	fn, sc := "script.js",  `
		const unimatrix01 : number = 1; 
		let x = {result: 6 * 7* unimatrix01, atime: new Date(), sin1: Math.sin(1.0), author: "me", coordinates: { x: 5, y: 6 } };
		let r = JSON.stringify(x, null, 2);

		r;
	`
	es15script := transpile(fn, sc)
	log.Println("\n", es15script)

	// --------------

	js = goja.New()
	log.Println("executing script")
	v, err := js.RunScript(fn, es15script)
	if err != nil {
		log.Fatalln(err)
	}

	log.Printf("%[1]T: %[1]v", v.Export())
}
