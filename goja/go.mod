module gitlab.com/lercher/badger/goja

go 1.12

require (
	github.com/dlclark/regexp2 v1.2.0 // indirect
	github.com/dop251/goja v0.0.0-20190912223329-aa89e6a4c733
	github.com/go-sourcemap/sourcemap v2.1.2+incompatible // indirect
	golang.org/x/text v0.3.2 // indirect
)
