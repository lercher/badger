package main

// win:
// set CGO_ENABLED=0
// go fmt . && go mod tidy && go run .

// go install github.com/dgraph-io/badger/v2/badger
// badger backup -t --dir db

import (
	"log"
	"runtime"

	badger "github.com/dgraph-io/badger/v2"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// Open the Badger database. It will be created if it doesn't exist.
	opt := badger.DefaultOptions("db")
	if !opt.Truncate && runtime.GOOS == "windows" {
		// https://github.com/dgraph-io/badger/issues/476
		opt.Truncate = true
		log.Println("[Windows] Truncate option set:", opt.Truncate)
	}
	db, err := badger.Open(opt)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = db.View(func(txn *badger.Txn) error {
		opt := badger.DefaultIteratorOptions
		it := txn.NewIterator(opt)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()
			item.Value(func(v []byte) error {
				log.Println("KEY", k, "VALUE", v)
				return nil
			})
		}
		return nil
	})
	log.Println("VIEW", err)

	err = db.Update(func(txn *badger.Txn) error {
		txn.Set([]byte{1}, []byte{1, 2, 3, 4, 5, 6})
		txn.Set([]byte{2}, []byte{6, 5, 4, 3, 2, 1, 0, 0, 0})
		return nil
	})
	log.Println("UPDATE", err)
	log.Fatalln("SIMULATED CRASH with os.Exit() call here")
}
