package main

// win:
// set CGO_ENABLED=0
// go fmt . && go mod tidy && go run .

// TODO: when changing the Owner or Creator of a dat
// we need to delete the old inversion entry for Owner and Creator

// TODO: we need to properly store the keys for items envelopes and inversion entries,
// best done with a flock of structs that are serialized with gob.

// TODO: gob probably isn't the right decision to store generic
// JSON data (i.e. map[string]interface{} inside map[string]interface{})
// b/c of its way to handle and gob.Register interface types
// an encoding/json might be better here

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	badger "github.com/dgraph-io/badger/v2"
)

type id [16]byte // a guid

type data struct {
	ID      id                     `json:"id,omitempty"`
	Title   string                 `json:"title,omitempty"`
	Owner   string                 `json:"owner,omitempty"`
	Creator string                 `json:"creator,omitempty"`
	Created time.Time              `json:"created,omitempty"`
	Changed time.Time              `json:"changed,omitempty"`
	Map     map[string]interface{} `json:"map,omitempty"`
}

const (
	prefixID       = byte('>')
	prefixENVELOPE = byte('(')
	prefixNAME     = byte('<')
	prefixVALUE    = byte('=')
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	gob.Register(map[string]interface{}{})

	// Open the Badger database. It will be created if it doesn't exist.
	opt := badger.DefaultOptions("db")
	if !opt.Truncate && runtime.GOOS == "windows" {
		// https://github.com/dgraph-io/badger/issues/476
		opt.Truncate = true
		log.Println("[Windows] Truncate option set:", opt.Truncate)
	}
	db, err := badger.Open(opt)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// a value with an id
	dat := data{
		ID:      [...]byte{11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26},
		Title:   "This is a title",
		Owner:   "MyOtherOwner",
		Creator: "MyCreator",
		Created: time.Date(2019, 11, 17, 2, 25, 0, 0, time.Local),
		Changed: time.Now(),
		Map:     map[string]interface{}{"abc": "ABC", "def": "DEF", "1234": 567890, "innermap": map[string]interface{}{"innerA": "A", "inner1": 1}},
	}

	// allocate a new int for key{1}
	seq, err := db.GetSequence([]byte{1}, 1)
	if err != nil {
		log.Println("SEQ", err)
	} else {
		seq.Release()
		nx, err := seq.Next()
		if err != nil {
			log.Println("SEQ-NEXT", err)
		} else {
			log.Println("SEQ", nx)
		}
	}

	// encode it
	keybuf := new(bytes.Buffer)
	keybuf.WriteByte(prefixID)
	keybuf.Write(dat.ID[:])

	// read all keys
	err = db.View(func(txn *badger.Txn) error {
		opt := badger.DefaultIteratorOptions
		it := txn.NewIterator(opt)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()
			item.Value(func(v []byte) error {
				log.Println("KEY", k, "VALUE", v)
				return nil
			})
		}
		return nil
	})
	if err != nil {
		log.Println(err)
	}

	// read the previous value and iterate over owner
	err = db.View(func(txn *badger.Txn) error {
		// read all Owner=MyOwner
		opt := badger.DefaultIteratorOptions
		opt.PrefetchValues = false // keys only
		it := txn.NewIterator(opt)
		defer it.Close()
		pbuf := new(bytes.Buffer)
		makeInversionPrefix(pbuf, "Creator", "MyCreator")
		prefix := pbuf.Bytes()
		for it.Seek(prefix); it.ValidForPrefix(prefix); it.Next() {
			item := it.Item()
			k := item.Key()
			id := k[len(prefix):]
			log.Println(string(prefix), id)
		}

		// read item
		item, err := txn.Get(keybuf.Bytes())
		if err != nil {
			return err
		}
		err = item.Value(func(v []byte) error {
			rd := bytes.NewReader(v)
			dec := gob.NewDecoder(rd)
			var d data
			err := dec.Decode(&d)
			if err != nil {
				return fmt.Errorf("gob.decode %d bytes %v: %v", len(v), v, err)
			}
			log.Println("Loaded version", item.Version(), ":", dat)
			return nil
		})
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		log.Println(err)
	}

	// store the value and an inversion index for owner and creator
	err = db.Update(func(txn *badger.Txn) error {
		// can't use the same buffer here, b/c txn.Set doesn't store it, it's done by db.Update
		dbuf := new(bytes.Buffer)
		enc := gob.NewEncoder(dbuf)
		err = enc.Encode(dat)
		if err != nil {
			return err
		}
		err := txn.Set(keybuf.Bytes(), dbuf.Bytes())
		if err != nil {
			return err
		}
		log.Println("stored", dat, "len", len(dbuf.Bytes()), ":", dbuf.Bytes())
		log.Printf("%q\n", string(dbuf.Bytes()))

		iown := new(bytes.Buffer)
		makeInversion(iown, "Owner", dat.Owner, dat.ID)
		err = txn.Set(iown.Bytes(), nil)
		if err != nil {
			return err
		}

		icr := new(bytes.Buffer)
		makeInversion(icr, "Creator", dat.Creator, dat.ID)
		err = txn.Set(icr.Bytes(), nil)
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		log.Println(err)
	}

	// GC
	log.Println("GC ...")
	for db.RunValueLogGC(0.7) == nil {
		// redo until err != nil, see https://github.com/dgraph-io/badger#garbage-collection
	}

	// Backup
	log.Println("BACKUP ...")
	f, err := os.Create("db.bak")
	if err != nil {
		log.Println(err)
	}
	defer f.Close()

	vers, err := db.Backup(f, 0)
	if err != nil {
		log.Println(err)
	}

	log.Println("Backup", f.Name(), "written, version #", vers)
}

func makeInversionPrefix(buf *bytes.Buffer, name, value string) {
	buf.WriteByte(prefixNAME)
	buf.WriteString(name)
	buf.WriteByte(prefixVALUE)
	buf.WriteString(value)
	buf.WriteByte(prefixID)
}

func makeInversion(buf *bytes.Buffer, name, value string, id id) {
	makeInversionPrefix(buf, name, value)
	buf.Write(id[:])
}
