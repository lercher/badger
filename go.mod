module gitlab.com/lercher/badger

go 1.12

require github.com/dgraph-io/badger/v2 v2.0.0
